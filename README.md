# arduino-handscanner

Interactive: Death / Nothing is lost
The visitor can start a movie by putting a hand in a place that has been given.
Script operation:

Setup() activates a timer that generates an interrupt 100 times per second. In this interrupt, the condition of the sensor is measured. If the same condition is measured in a number of successive interrupts, this is transmitted to the mainloop by means of a flag. This method prevents any spikes from the sensor from causing unwanted start-up of the film. MaxSensIntr adjusts the speed of switching.

In the loop() part of the script, the interrupt flag is checked. If so, the new state of the sensor is transmitted to the host (NUC) as keyboard events.

Connection sensor to Arduino: The sensor is powered by an external 5 volt power supply. The sensor is separated from the arduino by an opto coupler galvanic. On the sensor print, the signal from the optocoupler is carried out via the E (emitter) and C (collector) connections. It connects to the Arduino Gnd and C on input 1. (That's pin A0). A0 must be defined in setup as input with pull-up resistance.
