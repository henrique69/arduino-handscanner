//  ----------------------------------------------------------------------------------------------------------------
//
//  Interactive: de Dood / Niets gaat verloren
//
//  De bezoeker kan een filmpje starten door een hand op een daarvoor aagegeven plek te leggen.
//  
//  Werking script:
//  
//  In setup() wordt een timer geactiveerd die 100 keer per seconde een interrupt genereerd 
//  In deze interrupt wordt de toestand van de sensor gemeten. Als in een aantal opeenvolgende interrupts de zelfde toestand wordt gemeten
//  dan wordt dit middels een flag doorgegeven aan de mainloop. Deze methode voorkomt dat eventuele spikes uit de sensor voor ongewenst
//  starten van de film veroorzaakt. Met MaxSensIntr kan de snelheid van om-schakelen worden ingesteld.
//
//  in het loop() gedeelte van het script wordt gekeken of de interrupt flag actief is. Als dit zo is wordt de nieuwe toestand van de sensor 
//  als keyboard events aan de host (NUC) doorgegeven.
//
//  Aansluiting sensor op Arduino:
//  De sensor wordt gevoed door een externe 5 volt voeding. De sensor wordt dmv een opto coupler galvanisch van de arduino gescheiden.
//  Op de sensorprint wordt het signaal van de optocoupler naar buiten gevoerd via de E (emitter) en C (collector) aansluitingen.
//  E wordt aangesloten op de Arduino Gnd  en C op input 1. (Dat is pin A0). A0 moet in setup als input met pull-up weerstand gedefineerd worden.
//
//


#include <TimerOne.h>
#include "Keyboard.h"

#define Sensor A0

boolean IntrFlag = false;
volatile boolean Sensing = false; // in deze boolean variable wordt de toestand van de sensor bijgehouden.
#define MaxSensIntr 5
volatile int SensIntrCnt = 0;


void setup() {
  pinMode(Sensor,INPUT_PULLUP);
  Timer1.initialize(10000);// = elke 0.01 seconde een interrupt
  Timer1.attachInterrupt(T1Intr);
  // Serial.begin(115200);
  Keyboard.begin();
}

// Werking interrupt routine T1Intr.
//  Onderstaande routine wordt 100 keer per seconde uitgevoerd.
//  In de interruptroutine wordt gemeten of de sensor actief / niet actief is
//  Als de sensor MaxSensIntr keer een zelfde meting doet dan wordt de variable Sensing bijgewerkt en wordt middels een flag aan de mainloop doorgegeven dat 
//  er een verandering in de toestand van de sensor is gedetecteerd.

void T1Intr(void)
{
  if (digitalRead(Sensor) == HIGH) // Indien HIGH dan is de sensor niet actief (geen hand)
  {
    if (SensIntrCnt < MaxSensIntr)
    {
      SensIntrCnt++;
      if (SensIntrCnt == MaxSensIntr)
      {
        Sensing = false; // Sensor is nu officieel niet actief
        IntrFlag = true; // IntrFlag wordt true om in main loop te kunnen detecteren dat er een sensor event heeft plaatsgevonden
      }
       
    }
  }
  else // Sensor is wel actief (wel een hand);
    if (SensIntrCnt > 0)
    {
      SensIntrCnt--;
      if (SensIntrCnt == 0)
      {
        Sensing = true; // Sensor is nu officieel wel actief
        IntrFlag = true; // IntrFlag wordt true om in main loop te kunnen detecteren dat er een sensor event heeft plaatsgevonden
      }
    }
}



void loop() 
{
  if(IntrFlag)
  {
    IntrFlag = false;
    if(Sensing)
    {
 //     Serial.println("Sensor aan");
        Keyboard.print("R");
    }   
    else
    {
//      Serial.println("Sensor uit");
        Keyboard.print("S");
    }
  }
  delay(50);
}
// RSRSRSRSRSRSRSRS
